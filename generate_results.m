clear;
clc;
warning off;

%load image 
image = imread('H01.tif');

% load image 
% image = imread('PR6.png');

%copy original image 
copy_original_image = image;
%convert to gray
image = rgb2gray(image);
%contrast enhancement 
image = adapthisteq(image);
%convert image to double 
image = double(image);
%get the dimension of the matrix
[R, C] = size (image);
%initial centroids 
initial_centroids = [80 200];

% centroids for PR6.png image
% initial_centroids = [50 70];

%initialize zeros matrix for cluster one
cluster_one = zeros(R,C); 
%initialize zeros matrix for cluster two
cluster_two = zeros(R,C); 


while(true)
    old_centroids = initial_centroids; 
    %rows
    for i = 1:R
        %columns 
        for j = 1:C
            %obtain pixel value of (i, j) coordinates  
            pixel_coordinates = image(i, j);
            
            %calculate distance of coordinate from clusters 
            ab = abs (image(i,j) - initial_centroids);
            mn = find(ab == min(ab));

            %assign pixel coordinate (pixel_coordinates) to one of the clusters 
            if mn(1) == 1
                cluster_one (i, j) = pixel_coordinates; %1st cluster
            end
            if mn(1) == 2
                cluster_two (i, j) = pixel_coordinates; %2nd cluster
            end

        end
        
        disp('wait, it takes time');
        
    end
    
    
    %transform into vector cluster one
    cluster_one_vector= cluster_one(:);
    %sum vectors
    cluster_one_vector_sum = sum(cluster_one_vector);
    %find non zero elements in vector
    find_non_zero_cluster_one_vector = find(cluster_one_vector);
    %calculate the length of non zero elements
    length_cluster_one_vector = length(find_non_zero_cluster_one_vector);
    %new centroid
    new_centroid_cluster_one=cluster_one_vector_sum/length_cluster_one_vector;
    %round it up 
    new_centroid_cluster_one=round(new_centroid_cluster_one);
    
    
    %transform into vector cluster one
    cluster_two_vector= cluster_two(:);
    %sum vectors
    cluster_two_vector_sum = sum(cluster_two_vector);
    %find non zero elements in vector
    find_non_zero_cluster_two_vector = find(cluster_two_vector);
    %calculate the length of non zero elements
    length_cluster_two_vector = length(find_non_zero_cluster_two_vector);
    %new centroid
    new_centroid_cluster_two=cluster_two_vector_sum/length_cluster_two_vector;
    %round it up 
    new_centroid_cluster_two=round(new_centroid_cluster_two);
    
    

    %new centroids 
    initial_centroids = [new_centroid_cluster_one new_centroid_cluster_two];
    if(initial_centroids == old_centroids)
        break;
    end
end 

%labelling 
for i=1:R
   for j=1:C
     if cluster_one(i,j)>0
        cluster_one(i,j)=1;
     end
     if cluster_two(i,j)>0
        cluster_two(i,j)=2;
     end
   end
end

%sum clusters [one and two]
sum_up_clusters = (cluster_one + cluster_two);
%segmented image
segmented_image = label2rgb(sum_up_clusters);
%convert image from rgb to gray
segmented_image = rgb2gray(segmented_image);


%apply median filter 
segmented_image = medfilt2(segmented_image, [7 7]);
imwrite(segmented_image, 'segmented_image.tif');


%show original image source
figure, imshow(copy_original_image), title('Orginal image source');
%show segmented image k-means
figure, imshow(segmented_image), title('K-means Segmented image');
